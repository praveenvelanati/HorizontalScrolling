//
//  ViewController.swift
//  collection
//
//  Created by praveen velanati on 3/22/16.
//  Copyright © 2016 praveen velanati. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    var characters = [Character]()
    
    @IBOutlet weak var collection: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    collection.delegate = self
    collection.dataSource = self
    parseCharacterCSV()
    
    }

    
    func parseCharacterCSV() {
        
        let path = NSBundle.mainBundle().pathForResource("pokemon", ofType: "csv")!
        
        do {
            
            let csv = try CSV(contentsOfURL : path)
            let rows = csv.rows
            
            for row in rows {
               
                let pokeId = Int(row["id"]!)!
                let name = row["identifier"]!
                let person = Character(name : name , characterId : pokeId)
               
                characters.append(person)
                
            }
            
            
        } catch let err as NSError {
            
            print(err.localizedDescription)
        }
        
        
        
        
    }
    

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if let cell = collection.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as? CollectionCell {
            
            let person : Character!
            
            person = characters[indexPath.row]
            
            print(person.characterId)
            print(person.name)
            cell.configureCell(person)
            print(cell.namelabel.text)
            return cell
            
            
        }
        
        return UICollectionViewCell()
        
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
       
        return characters.count
    }
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        
        return 1
    }
    
    
      
    
    
    

}

