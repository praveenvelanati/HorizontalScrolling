//
//  CollectionCell.swift
//  collection
//
//  Created by praveen velanati on 3/22/16.
//  Copyright © 2016 praveen velanati. All rights reserved.
//

import UIKit

class CollectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var cellImage: UIImageView!
    
    @IBOutlet weak var namelabel: UILabel!
    
    
    var character : Character!
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        layer.cornerRadius = 5.0
    }
    
    
    func configureCell(character : Character) {
        
        self.character = character
        
        namelabel.text = character.name
        cellImage.image = UIImage(named: "\(self.character.characterId)")
        
        
        
    }
    
    
    
}
