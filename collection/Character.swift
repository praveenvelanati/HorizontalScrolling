//
//  Character.swift
//  collection
//
//  Created by praveen velanati on 3/22/16.
//  Copyright © 2016 praveen velanati. All rights reserved.
//

import Foundation

class  Character {

    private var _name : String!
    private var _characterId : Int!
    
    
    var name : String {
        
        return _name
    }
    
    
    var characterId : Int {
        
        return _characterId
    }
    
    
    init(name : String , characterId : Int) {
        
        self._name = name
        self._characterId = characterId
        
        
        
    }
    
    
    
}